library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity dff_tb is
  generic(
    formal_g : boolean := false
  );
end dff_tb;

architecture tb of dff_tb is

  constant clk_period : time := 5000 ps;
  constant debug : boolean := false;

  signal reset : std_logic;
  signal clk : std_logic;
  signal d : std_logic;
  signal q : std_logic;
  signal simulation_ended : boolean;

  component dff is
    generic(
      formal_g : boolean := false
    );
    port(
      reset : in std_logic;
      clk : in std_logic;
      d : in std_logic;
      q : out std_logic
    );
  end component dff;

begin

  clk_proc : process is
  begin
    if simulation_ended = false then
      clk <= '1';
      wait for clk_period/2;
      clk <= '0';
      wait for clk_period/2;
    else
      wait;
    end if;
  end process clk_proc;

  main_proc : process is

    procedure cycle
    (
      constant count : in natural
    ) is
      variable counter : integer;
    begin
      counter := 0;
      if count > 1 then
        for x in 0 to count - 1 loop
          wait until clk'event and clk = '1';
          counter := counter + 1;
          if debug = true then
            report "counter = " & integer'image(counter);
          end if;
        end loop;
      else -- if count = {0,1}
        wait until clk'event and clk = '1';
        counter := counter + 1;
        if debug = true then
          report "counter = " & integer'image(counter);
        end if;
      end if;
    end procedure cycle;

  begin
    -- Print testbench generics
    report "Testbench generics:";
    report "  formal_g = " & boolean'image(formal_g);

    -- Initialize testbench
    simulation_ended <= false;
    d <= '0';
    reset <= '0';
    cycle(2);

    report "Start of simulation";
    cycle(1);

    report "Reset DUT";
    reset <= '1';
    cycle(4);
    reset <= '0';
    cycle(4);

    report "Test 1";
    d <= '1';
    cycle(1);

    report "Test 2";
    d <= '0';
    cycle(1);

    report "Test 3";
    d <= '1';
    cycle(10);

    report "Test 4";
    d <= '0';
    cycle(10);

    report "Test 5";
    d <= '1';
    cycle(100);

    report "Test 6";
    d <= '0';
    cycle(100);

    simulation_ended <= true;
    report "End of testbench";
    wait;
  end process main_proc;

  dut : dff
  generic map(
    formal_g => formal_g
  )
  port map(
    reset => reset,
    clk => clk,
    d => d,
    q => q
  );
end tb;
