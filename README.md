# fusesoc_example

My personal FuseSoC learning example repo

## Commands

Assuming [fusesoc](https://github.com/olofk/fusesoc) is being installed already,
do the following to run the simulation:

> fusesoc run --target=sim --tool=ghdl dff
